const express = require('express');
const router = express.Router();
const {
  getFiles,
  createFile,
  getFileByFilename
} = require('../controllers/filesController');

router.route('/').get(getFiles).post(createFile);
router.route('/:filename').get(getFileByFilename);

module.exports = router;
